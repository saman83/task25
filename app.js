const { Sequelize, QueryTypes } = require('sequelize');

//Load our enviroment variables from the .env file
require('dotenv').config();

//write new instance of sequelize. Configuration will contain the database, username, password, etc..
//Process.env is an object we can use to add hidden variables to our node projects
const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

//Promise based package

async function connect() {
    try {
        
        await sequelize.authenticate();
        console.log('Connection established');

        const countries = await sequelize.query('SELECT * FROM country LIMIT 5', {
            type: QueryTypes.SELECT
        });
        
        const countryNames = countries.map(country => country.Name)

        const searchResults = countries.filter(country => country.Name.indexOf());

        console.log(countryNames);
        
        //************************************************ */

        const cities = await sequelize.query('SELECT * FROM city LIMIT 5', {
            type: QueryTypes.SELECT
        });
        
        const cityNames = cities.map(city => city.Name)

        const searchResults2 = cities.filter(cities => cities.Name.indexOf());

        console.log(cityNames);

        //************************************************ */

        const languages = await sequelize.query('SELECT * FROM countrylanguage LIMIT 5', {
            type: QueryTypes.SELECT
        });
        
        const languageNames = languages.map(str => str.Language)

        // const searchResults3 = languages.filter(languages => languages.countrylanguage.indexOf());

        console.log(languageNames);


    } catch (e) {
        console.log(e);
        
    }
}

connect();
